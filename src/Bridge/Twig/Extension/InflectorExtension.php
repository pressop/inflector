<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Inflector\Bridge\Twig\Extension;

use Pressop\Component\Inflector\Inflector;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class InflectorExtension
 *
 * @author Benjamin Georgeault
 */
class InflectorExtension extends AbstractExtension
{
    /**
     * @inheritDoc
     */
    public function getFilters()
    {
        return [
            new TwigFilter('tableize', [Inflector::class, 'tableize']),
            new TwigFilter('classify', [Inflector::class, 'classify']),
            new TwigFilter('camelize', [Inflector::class, 'camelize']),
            new TwigFilter('ucwords', [Inflector::class, 'ucwords']),
            new TwigFilter('pluralize', [Inflector::class, 'pluralize']),
            new TwigFilter('singularize', [Inflector::class, 'singularize']),
            new TwigFilter('basename', [Inflector::class, 'basename']),
        ];
    }
}
