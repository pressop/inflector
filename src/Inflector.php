<?php

/*
 * This file is part of the public_html package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Inflector;

use Doctrine\Common\Inflector\Inflector as BaseInflector;

/**
 * Class Inflector
 *
 * @author Benjamin Georgeault
 */
class Inflector extends BaseInflector
{
    /**
     * @param string $path
     * @return string
     * @deprecated
     */
    public static function basename(string $path): string
    {
        return FileInflector::basename($path);
    }

    /**
     * @param string $str
     * @return string
     * @see \Symfony\Component\Form\FormRenderer::humanize
     */
    public static function humanize(string $str): string
    {
        return ucfirst(strtolower(trim(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', ' '], $str))));
    }

    /**
     * @param string $str
     * @param string $word
     * @return string
     */
    public static function trimWord(string $str, string $word): string
    {
        return preg_replace('/' . preg_quote($word) . '$/', '', $str);
    }

    /**
     * @param string $str
     * @return string
     */
    public static function hyphenify(string $str): string
    {
        return preg_replace('/[_\s]+/', '-', self::tableize($str));
    }

    /**
     * @param string $str
     * @return string
     */
    public static function underscore(string $str): string
    {
        return strtolower(preg_replace(['/([A-Z]+)([A-Z][a-z])/', '/([a-z\d])([A-Z])/'], ['\\1_\\2', '\\1_\\2'], str_replace('_', '.', $str)));
    }
}
