<?php

/*
 * This file is part of the inflector package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Inflector;

/**
 * Class FileInflector
 *
 * @author Benjamin Georgeault
 */
class FileInflector
{
    /**
     * @param string $path
     * @return string
     */
    public static function basename(string $path): string
    {
        return basename(self::slashify($path));
    }

    /**
     * @param string $path
     * @return string
     */
    public static function dirname(string $path): string
    {
        return dirname(self::slashify($path));
    }

    /**
     * @param string $path
     * @return string
     */
    public static function slashify(string $path): string
    {
        return str_replace('\\', '/', $path);
    }

    /**
     * @param string $path
     * @return string
     */
    public static function realpath(string $path): string
    {
        $realPath = realpath($path);
        return false === $realPath ? $path : $realPath;
    }

    /**
     * @param string $path
     * @param bool $multiDotted
     * @return string
     */
    public static function trimExtension(string $path, bool $multiDotted = false): string
    {
        if ($multiDotted) {
            return preg_replace('/(\\.[^.\\s]{1,5})+$/', '', $path);
        }

        return preg_replace('/\\.[^.\\s]{1,5}$/', '', $path);
    }

    /**
     * @param string $path
     * @param string $relativeTo
     * @return string
     */
    public static function relativeTo(string $path, string $relativeTo): string
    {
        $path = rtrim(self::slashify($path), '/');
        $relativeTo = rtrim(self::slashify($relativeTo), '/');

        $pathParts = explode('/', $path);
        $relativeToParts = explode('/', $relativeTo);

        $resultParts = $relativeToParts;
        $countPathParts = count($pathParts);

        foreach($pathParts as $depth => $dir) {
            if (isset($relativeToParts[$depth]) && $dir === $relativeToParts[$depth]) {
                array_shift($resultParts);
            } else {
                $remaining = $countPathParts - $depth;

                if ($remaining > 1) {
                    $padLength = (count($resultParts) + $remaining - 1) * -1;
                    $resultParts = array_pad($resultParts, $padLength, '..');
                    break;
                } elseif (0 < count($resultParts)) {
                    $resultParts[0] = '/' . $resultParts[0];
                }
            }
        }

        return implode('/', $resultParts);
    }

    /**
     * @param string $path
     * @param string $relativeFrom
     * @return string
     */
    public static function relativeFrom(string $path, string $relativeFrom): string
    {
        return self::relativeTo($relativeFrom, $path);
    }
}
