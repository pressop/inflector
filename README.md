Inflector
=============

Extend Doctrine's inflector.

## Install

```
composer require pressop/inflector
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
